package com.amazonaws.lambda.transactionlist;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CSVInput;
import com.amazonaws.services.s3.model.CSVOutput;
import com.amazonaws.services.s3.model.CompressionType;
import com.amazonaws.services.s3.model.ExpressionType;
import com.amazonaws.services.s3.model.FileHeaderInfo;
import com.amazonaws.services.s3.model.InputSerialization;
import com.amazonaws.services.s3.model.JSONOutput;
import com.amazonaws.services.s3.model.OutputSerialization;
import com.amazonaws.services.s3.model.SelectObjectContentEvent;
import com.amazonaws.services.s3.model.SelectObjectContentEventVisitor;
import com.amazonaws.services.s3.model.SelectObjectContentRequest;
import com.amazonaws.services.s3.model.SelectObjectContentResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.IOUtils;

public class LambdaFunctionHandler implements RequestHandler<Object, String> {
	
	private static final String BUCKET_NAME = "cc-data-for-poc";
    private static final String CSV_OBJECT_KEY = "purchase_card_2014.csv.gz";
    private static final String S3_SELECT_RESULTS_PATH = "${my-s3-select-results-path}";
//    private static final String QUERY = "select * from S3Object s limit 5";
    private static final String QUERY = "select * from s3object s where s.\"Cardholder Last Name\" = 'Massey' and s.\"Cardholder First Initial\" = 'T'";


    @Override
    public String handleRequest(Object input, Context context) {
        context.getLogger().log("Input: " + input);
        final AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

        SelectObjectContentRequest request = generateBaseCSVRequest(BUCKET_NAME, CSV_OBJECT_KEY, (LinkedHashMap<String,String>)input);
        final AtomicBoolean isResultComplete = new AtomicBoolean(false);

//        try (OutputStream fileOutputStream = new FileOutputStream(new File ("/Users/mkurman/Desktop/s3select/output.txt"));
          try (SelectObjectContentResult result = s3Client.selectObjectContent(request)) {
            InputStream resultInputStream = result.getPayload().getRecordsInputStream(
                    new SelectObjectContentEventVisitor() {
                        @Override
                        public void visit(SelectObjectContentEvent.StatsEvent event)
                        {
                            System.out.println(
                                    "Received Stats, Bytes Scanned: " + event.getDetails().getBytesScanned()
                                            +  " Bytes Processed: " + event.getDetails().getBytesProcessed());
                        }

                        /*
                         * An End Event informs that the request has finished successfully.
                         */
                        @Override
                        public void visit(SelectObjectContentEvent.EndEvent event)
                        {
                            isResultComplete.set(true);
                            System.out.println("Received End Event. Result is complete.");
                        }
                    }
            );
            StringBuffer sb = new StringBuffer();
            sb.append("{ \"transactions\":[");
            sb.append(IOUtils.toString(resultInputStream));
            sb.append("]}");
//            IOUtils.toString(resultInputStream);
//            IOUtils.copy(resultInputStream,fileOutputStream);
            String tmpString = sb.toString();
            String newString = tmpString.replace("\n", "");
            String newNewString = newString.replace("}{", "},{");
            return newNewString;
        }catch(Exception e){
        	e.printStackTrace();
        }

        /*
         * The End Event indicates all matching records have been transmitted.
         * If the End Event is not received, the results may be incomplete.
         */
        if (!isResultComplete.get()) {
//            throw new Exception("S3 Select request was incomplete as End Event was not received.");
        	System.out.println("S3 Select request was incomplete as End Event was not received");
        }
        return "String";
    }
    private static SelectObjectContentRequest generateBaseCSVRequest(String bucket, String key, LinkedHashMap<String,String> inputVars) {
        SelectObjectContentRequest request = new SelectObjectContentRequest();
        request.setBucketName(bucket);
        request.setKey(key);
        String queryBuild = "select * from s3object s where s.\"cardholder_last_name\" = '"+inputVars.get("lastname")+"' and s.\"cardholder_first_initial\" = '"+inputVars.get("firstinitial")+"'";
        request.setExpression(queryBuild);
        request.setExpressionType(ExpressionType.SQL);

        InputSerialization inputSerialization = new InputSerialization();
        CSVInput csvInput = new CSVInput();
        csvInput.setFileHeaderInfo(FileHeaderInfo.USE);
        inputSerialization.setCsv(csvInput);
        inputSerialization.setCompressionType(CompressionType.GZIP);
        request.setInputSerialization(inputSerialization);

        OutputSerialization outputSerialization = new OutputSerialization();
        outputSerialization.setJson(new JSONOutput());
        request.setOutputSerialization(outputSerialization);

        return request;
    }
}
